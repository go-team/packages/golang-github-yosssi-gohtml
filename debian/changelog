golang-github-yosssi-gohtml (0.0~git20180130.97fbf36-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Set upstream metadata fields: Bug-Submit.
  * Apply multi-arch hints.
      + golang-github-yosssi-gohtml-dev: Add Multi-Arch: foreign.
      (Closes: #984514)

 -- Dylan Aïssi <daissi@debian.org>  Tue, 19 Nov 2024 22:41:07 +0100

golang-github-yosssi-gohtml (0.0~git20180130.97fbf36-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 13:20:08 +0100

golang-github-yosssi-gohtml (0.0~git20180130.97fbf36-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 0.0~git20180130.97fbf36
  * Apply "cme fix dpkg" to d/control and d/copyright,
    bumping Standards-Version to 4.1.3,
    setting Priority to optional,
    adding "Testsuite: autopkgtest-pkg-go",
    changing license name from MIT to Expat, etc.

 -- Anthony Fok <foka@debian.org>  Tue, 03 Apr 2018 14:20:58 -0600

golang-github-yosssi-gohtml (0.0~git20170501.0.0cb9872-1) unstable; urgency=medium

  * New upstream version.
  * Use debhelper (>= 10).
  * Bump Standards-Version to 4.0.0:
    Use https form of the copyright-format URL in debian/copyright.
  * Add "Testsuite: autopkgtest-pkg-go" to debian/control.

 -- Anthony Fok <foka@debian.org>  Sun, 09 Jul 2017 23:25:12 -0600

golang-github-yosssi-gohtml (0.0~git20150923.0.ccf383e-4) unstable; urgency=medium

  * Remove double escape backslashes in debian/watch

 -- Anthony Fok <foka@debian.org>  Mon, 02 Jan 2017 02:21:35 -0700

golang-github-yosssi-gohtml (0.0~git20150923.0.ccf383e-3) unstable; urgency=medium

  * Refresh debian/control
    - Bump Standards-Version to 3.9.8 (no change)
    - Use secure https URL for Vcs-Git field too
    - Replace golang-go with golang-any in Build-Depends
    - Remove "golang-go | gccgo" from Depends field
  * Add debian/watch file

 -- Anthony Fok <foka@debian.org>  Fri, 10 Jun 2016 14:25:08 -0600

golang-github-yosssi-gohtml (0.0~git20150923.0.ccf383e-2) unstable; urgency=medium

  * Allow package to be installed with gccgo where golang-go is unavailable
  * Update debian/copyright

 -- Anthony Fok <foka@debian.org>  Fri, 15 Jan 2016 00:41:35 -0700

golang-github-yosssi-gohtml (0.0~git20150923.0.ccf383e-1) unstable; urgency=medium

  * New upstream snapshot
  * Upstream has merged our Pull Request to "Migrate import path from
    code.google.com to golang.org", removing Debian patches.

 -- Anthony Fok <foka@debian.org>  Tue, 03 Nov 2015 08:44:06 -0700

golang-github-yosssi-gohtml (0.0~git20141007.0.48cbef8-1) unstable; urgency=medium

  * Initial release (Closes: #799812)
  * Migrate import path from code.google.com to golang.org

 -- Anthony Fok <foka@debian.org>  Tue, 22 Sep 2015 15:48:31 -0600
